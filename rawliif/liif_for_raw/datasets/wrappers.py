#Code adapted from https://github.com/goutamgmb/deep-burst-sr and https://github.com/yinboc/liif

import functools
import random
import math
from PIL import Image

from . import rgb2raw

import cv2
import numpy as np
import torch
from torch.utils.data import Dataset
from torchvision import transforms

from datasets import register
from utils import to_pixel_samples

import threading

def resize_fn(img, size): #resizes to target size and transforms entries from RGB (0 - 255) to [0,1]
    """
    Takes an input image tensor, unnormalized or not, and resizes it, where the dimension (height or width) that 
    is smaller get set to input parameter size. In our case, they have the same size, so no need to worry.
    The code looks very ugly, but doing it that way using PIL uses antialiasing for downsampling and is the way to go. 
    See the warning on https://pytorch.org/vision/main/generated/torchvision.transforms.Resize.html for more information.
    """
    return transforms.ToTensor()(
        transforms.Resize(size, Image.BICUBIC)(
            transforms.ToPILImage()(img))) #HxWxC, same as torch.read_image


def raw_conv(img, downsample_factor= 1, interpolation_type="bilinear"):
    """
    Does demosaicing and adds noise e.g. creates raw data, all other steps are already 
    done before this function is called, such as gamma expansion etc.
    """
    image_burst, image, image_burst_rgb, flow_vectors, meta_info = rgb2raw.rgb2rawburst(
        img, lr_size = 48, burst_size = 1, downsample_factor=downsample_factor, 
        burst_transformation_params = None,
        image_processing_params={'random_ccm': False, 'random_gains': False, 'smoothstep': False, 'gamma': False, 'add_noise': True}, interpolation_type=interpolation_type)
    return image_burst.squeeze()

@register('raw-burst-sr-implicit-downsampled')
class RawBurstSRImplicitDownsampled(Dataset):
    def __init__(self, dataset, inp_size, scale_min=1, scale_max=None,
                 augment=False, sample_q=None, return_meta=False, burst_size=5, burst_transformation_params= None):
        self.dataset = dataset
        self.inp_size = inp_size
        self.scale_min = scale_min
        if scale_max is None:
            self.scale_max = scale_min
        else:
            self.scale_max = scale_max
        self.augment = augment
        self.sample_q = sample_q
        self.return_meta = return_meta
        self.burst_size = burst_size

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        img = self.dataset[idx]
        s = random.uniform(self.scale_min, self.scale_max)
        #define sizes
        w_lr = self.inp_size
        w_hr = math.floor((w_lr * s)/2)*2
        x0 = random.randint(0, img.shape[-2] - w_hr)
        y0 = random.randint(0, img.shape[-1] - w_hr)
        crop_hr = img[:, x0: x0 + w_hr, y0: y0 + w_hr].clone().detach()    
        #print("crop_hr shape: ", crop_hr.shape)    
        raw_burst, hr_lin, image_burst_rgb, flow_vectors, meta_info = rgb2raw.rgb2rawburst(
        crop_hr, w_lr, burst_size = self.burst_size, downsample_factor=s, 
        burst_transformation_params={'max_translation': 5.0,  'max_rotation': 1.0,  'max_shear': 0.0,  'max_scale': 0.0,  'border_crop': 5},
        image_processing_params={'random_ccm': True, 'random_gains': True, 'smoothstep': True, 'gamma': True, 'add_noise': True}, interpolation_type="bilinear")

        #print("hr_lin.shape: ", hr_lin.shape)

        hr_coord, hr_rgb = to_pixel_samples(hr_lin.contiguous())

        if self.sample_q is not None:
            sample_lst = np.random.choice(
                len(hr_coord), self.sample_q, replace=False)
            hr_coord = hr_coord[sample_lst]
            hr_rgb = hr_rgb[sample_lst]
        
        #print("wrappers.py: ", raw_burst.shape)

        cell = torch.ones_like(hr_coord)
        cell[:, 0] *= 2 / hr_lin.shape[-2]
        cell[:, 1] *= 2 / hr_lin.shape[-1]
        
        if self.return_meta is False:
            return {
                'inp': raw_burst,
                'coord': hr_coord,
                'cell': cell,
                'gt': hr_rgb,
            }
        else: 
            return {
                'inp': raw_burst,
                'coord': hr_coord,
                'cell': cell,
                'gt': hr_rgb,
                'meta_info' : meta_info
            }    
    
@register('full-res-raw-sr-implicit-downsampled')
class FullResRawSRImplicitDownsampled(Dataset):
    def __init__(self, dataset, inp_size=None, scale_min=1, scale_max=None,
                 augment=False, sample_q=None, return_meta=False, batch_size=4):
        self.dataset = dataset
        self.inp_size = inp_size
        self.scale_min = scale_min
        if scale_max is None:
            scale_max = scale_min
        self.scale_max = scale_max
        self.augment = augment
        self.sample_q = sample_q
        self.return_meta = return_meta
        self.batch_size = batch_size
        self.counter = -1
        self.s = 0
        self.threadLock = threading.Lock()

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        img = self.dataset[idx]
        #s = random.uniform(self.scale_min, self.scale_max)
        with self.threadLock:
            self.counter = (self.counter+1)%(self.batch_size)
            if (self.counter == 0): 
                self.s = (self.s)%(self.scale_max) + 1

        #print((self.counter, self.s))

        if self.inp_size is None: #if input size is variable
            h_lr = math.floor(img.shape[-2] / self.s + 1e-9)
            w_lr = math.floor(img.shape[-1] / self.s + 1e-9)
            img = img[:, :round(h_lr * self.s), :round(w_lr * self.s)] # assume round int
            img_down = resize_fn(img, (h_lr, w_lr))
            crop_lr, crop_hr = img_down, img
        else: #if input size is given
            w_lr = self.inp_size
            w_hr = math.floor((w_lr * self.s)/2)*2
            x0 = random.randint(0, img.shape[-2] - w_hr)
            y0 = random.randint(0, img.shape[-1] - w_hr)
            crop_hr = img[:, x0: x0 + w_hr, y0: y0 + w_hr]
        

        # Generate random gains
        rgb_gain, red_gain, blue_gain = rgb2raw.random_gains()

        # Generate random CCM
        rgb2cam = rgb2raw.random_ccm()

        cam2rgb = rgb2cam.inverse()

        # Approximately inverts global tone mapping.
        crop_hr = rgb2raw.invert_smoothstep(crop_hr)

        # Inverts gamma compression.
        crop_hr = rgb2raw.gamma_expansion(crop_hr)

        # Apply color correction.
        crop_hr = rgb2raw.apply_ccm(crop_hr, rgb2cam)

        # Approximately inverts white balance and brightening.
        crop_hr = rgb2raw.safe_invert_gains(crop_hr, rgb_gain, red_gain, blue_gain)

        # Clip saturated pixels.
        crop_hr = crop_hr.clamp(0.0, 1.0)

        #print("crop_hr shape ", crop_hr.shape)
        crop_lr = resize_fn(crop_hr, w_lr)
        #print("crop_lr shape ", crop_lr.shape)

            #print(crop_hr)
        if self.augment:
            hflip = random.random() < 0.5
            vflip = random.random() < 0.5
            dflip = random.random() < 0.5

            def augment(x):
                if hflip:
                    x = x.flip(-2)
                if vflip:
                    x = x.flip(-1)
                if dflip:
                    x = x.transpose(-2, -1)
                return x

            crop_lr = augment(crop_lr)
            crop_hr = augment(crop_hr)

        hr_coord, hr_rgb = to_pixel_samples(crop_hr.contiguous())

        if self.sample_q is not None:
            sample_lst = np.random.choice(
                len(hr_coord), self.sample_q, replace=False)
            hr_coord = hr_coord[sample_lst]
            hr_rgb = hr_rgb[sample_lst]
        

        cell = torch.ones_like(hr_coord)
        cell[:, 0] *= 2 / crop_hr.shape[-2]
        cell[:, 1] *= 2 / crop_hr.shape[-1]

        raw_crop_lr = rgb2raw.mosaic(crop_lr)

        shot_noise_level, read_noise_level = rgb2raw.random_noise_levels()
        raw_crop_lr = rgb2raw.add_noise(raw_crop_lr, shot_noise_level, read_noise_level)
        
        if self.return_meta is False:
            return {
                'inp': raw_crop_lr,
                'coord': hr_coord,
                'cell': cell,
                'gt': hr_rgb,
            }
        else: 
            return {
                'inp': raw_crop_lr,
                'coord': hr_coord,
                'cell': cell,
                'gt': hr_rgb,
                'meta_info' : {'rgb2cam': rgb2cam, 'cam2rgb': cam2rgb, 'rgb_gain': rgb_gain, 'red_gain': red_gain,
                     'blue_gain': blue_gain,
                     'shot_noise_level': shot_noise_level, 'read_noise_level': read_noise_level}
            }

@register('raw-sr-implicit-downsampled')
class RawSRImplicitDownsampled(Dataset):

    def __init__(self, dataset, inp_size=None, scale_min=1, scale_max=None,
                 augment=False, sample_q=None, return_meta=False, burst_length=8):
        self.dataset = dataset
        self.inp_size = inp_size
        self.scale_min = scale_min
        if scale_max is None:
            scale_max = scale_min
        self.scale_max = scale_max
        self.augment = augment
        self.sample_q = sample_q
        self.return_meta = return_meta
        self.burst_length = burst_length

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        img = self.dataset[idx]
        s = random.uniform(self.scale_min, self.scale_max)

        if self.inp_size is None: #if input size is variable
            h_lr = math.floor(img.shape[-2] / s + 1e-9)
            w_lr = math.floor(img.shape[-1] / s + 1e-9)
            img = img[:, :round(h_lr * s), :round(w_lr * s)] # assume round int
            img_down = resize_fn(img, (h_lr, w_lr))
            crop_lr, crop_hr = img_down, img
        else: #if input size is given
            w_lr = self.inp_size
            w_hr = math.floor((w_lr * s)/2)*2
            x0 = random.randint(0, img.shape[-2] - w_hr)
            y0 = random.randint(0, img.shape[-1] - w_hr)
            crop_hr = img[:, x0: x0 + w_hr, y0: y0 + w_hr]
        

        # Generate random gains
        rgb_gain, red_gain, blue_gain = rgb2raw.random_gains()

        # Generate random CCM
        rgb2cam = rgb2raw.random_ccm()

        cam2rgb = rgb2cam.inverse()

        # Approximately inverts global tone mapping.
        crop_hr = rgb2raw.invert_smoothstep(crop_hr)

        # Inverts gamma compression.
        crop_hr = rgb2raw.gamma_expansion(crop_hr)

        # Apply color correction.
        crop_hr = rgb2raw.apply_ccm(crop_hr, rgb2cam)

        # Approximately inverts white balance and brightening.
        crop_hr = rgb2raw.safe_invert_gains(crop_hr, rgb_gain, red_gain, blue_gain)

        # Clip saturated pixels.
        crop_hr = crop_hr.clamp(0.0, 1.0)

        #print("crop_hr shape ", crop_hr.shape)
        crop_lr = resize_fn(crop_hr, w_lr)
        #print("crop_lr shape ", crop_lr.shape)

            #print(crop_hr)
        if self.augment:
            hflip = random.random() < 0.5
            vflip = random.random() < 0.5
            dflip = random.random() < 0.5

            def augment(x):
                if hflip:
                    x = x.flip(-2)
                if vflip:
                    x = x.flip(-1)
                if dflip:
                    x = x.transpose(-2, -1)
                return x

            crop_lr = augment(crop_lr)
            crop_hr = augment(crop_hr)

        hr_coord, hr_rgb = to_pixel_samples(crop_hr.contiguous())

        if self.sample_q is not None:
            sample_lst = np.random.choice(
                len(hr_coord), self.sample_q, replace=False)
            hr_coord = hr_coord[sample_lst]
            hr_rgb = hr_rgb[sample_lst]
        
        #print("wrappers.py: ", hr_rgb.shape)

        cell = torch.ones_like(hr_coord)
        cell[:, 0] *= 2 / crop_hr.shape[-2]
        cell[:, 1] *= 2 / crop_hr.shape[-1]

        raw_crop_lr = rgb2raw.mosaic(crop_lr)

        shot_noise_level, read_noise_level = rgb2raw.random_noise_levels()
        raw_crop_lr = rgb2raw.add_noise(raw_crop_lr, shot_noise_level, read_noise_level)
        
        if self.return_meta is False:
            return {
                'inp': raw_crop_lr,
                'coord': hr_coord,
                'cell': cell,
                'gt': hr_rgb,
            }
        else: 
            return {
                'inp': raw_crop_lr,
                'coord': hr_coord,
                'cell': cell,
                'gt': hr_rgb,
                'meta_info' : {'rgb2cam': rgb2cam, 'cam2rgb': cam2rgb, 'rgb_gain': rgb_gain, 'red_gain': red_gain,
                     'blue_gain': blue_gain,
                     'shot_noise_level': shot_noise_level, 'read_noise_level': read_noise_level}
            }

@register('sr-implicit-paired')
class SRImplicitPaired(Dataset):
    """
    Dataset made up of pairs of LR and HR images, 
    """
    def __init__(self, dataset, inp_size=None, augment=False, sample_q=None):
        self.dataset = dataset
        self.inp_size = inp_size
        self.augment = augment
        self.sample_q = sample_q

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        img_lr, img_hr = self.dataset[idx]

        s = img_hr.shape[-2] // img_lr.shape[-2] # floor division
        if self.inp_size is None:
            h_lr, w_lr = img_lr.shape[-2:]
            img_hr = img_hr[:, :h_lr * s, :w_lr * s]
            crop_lr, crop_hr = img_lr, img_hr
        else:
            w_lr = self.inp_size
            x0 = random.randint(0, img_lr.shape[-2] - w_lr)
            y0 = random.randint(0, img_lr.shape[-1] - w_lr)
            crop_lr = img_lr[:, x0: x0 + w_lr, y0: y0 + w_lr]
            w_hr = w_lr * s
            x1 = x0 * s
            y1 = y0 * s
            crop_hr = img_hr[:, x1: x1 + w_hr, y1: y1 + w_hr]

        if self.augment:
            hflip = random.random() < 0.5
            vflip = random.random() < 0.5
            dflip = random.random() < 0.5

            def augment(x):
                if hflip:
                    x = x.flip(-2)
                if vflip:
                    x = x.flip(-1)
                if dflip:
                    x = x.transpose(-2, -1)
                return x

            crop_lr = augment(crop_lr)
            crop_hr = augment(crop_hr)

        hr_coord, hr_rgb = to_pixel_samples(crop_hr.contiguous())

        if self.sample_q is not None:
            sample_lst = np.random.choice(
                len(hr_coord), self.sample_q, replace=False)
            hr_coord = hr_coord[sample_lst]
            hr_rgb = hr_rgb[sample_lst]

        cell = torch.ones_like(hr_coord)
        cell[:, 0] *= 2 / crop_hr.shape[-2]
        cell[:, 1] *= 2 / crop_hr.shape[-1]

        return {
            'inp': crop_lr,
            'coord': hr_coord,
            'cell': cell,
            'gt': hr_rgb
        }



@register('sr-implicit-downsampled')
class SRImplicitDownsampled(Dataset):
    def __init__(self, dataset, inp_size=None, scale_min=1, scale_max=None,
                 augment=False, sample_q=None):
        self.dataset = dataset
        self.inp_size = inp_size
        self.scale_min = scale_min
        if scale_max is None:
            scale_max = scale_min
        self.scale_max = scale_max
        self.augment = augment
        self.sample_q = sample_q

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        img = self.dataset[idx]
        s = random.uniform(self.scale_min, self.scale_max)

        if self.inp_size is None:
            h_lr = math.floor(img.shape[-2] / s + 1e-9)
            w_lr = math.floor(img.shape[-1] / s + 1e-9)
            img = img[:, :round(h_lr * s), :round(w_lr * s)] # assume round int
            img_down = resize_fn(img, (h_lr, w_lr))
            crop_lr, crop_hr = img_down, img
        else:
            w_lr = self.inp_size
            w_hr = round(w_lr * s)
            x0 = random.randint(0, img.shape[-2] - w_hr)
            y0 = random.randint(0, img.shape[-1] - w_hr)
            crop_hr = img[:, x0: x0 + w_hr, y0: y0 + w_hr]
            crop_lr = resize_fn(crop_hr, w_lr)

        if self.augment:
            hflip = random.random() < 0.5
            vflip = random.random() < 0.5
            dflip = random.random() < 0.5

            def augment(x):
                if hflip:
                    x = x.flip(-2)
                if vflip:
                    x = x.flip(-1)
                if dflip:
                    x = x.transpose(-2, -1)
                return x

            crop_lr = augment(crop_lr)
            crop_hr = augment(crop_hr)

        hr_coord, hr_rgb = to_pixel_samples(crop_hr.contiguous())

        if self.sample_q is not None:
            sample_lst = np.random.choice(
                len(hr_coord), self.sample_q, replace=False)
            hr_coord = hr_coord[sample_lst]
            hr_rgb = hr_rgb[sample_lst]

        cell = torch.ones_like(hr_coord)
        cell[:, 0] *= 2 / crop_hr.shape[-2]
        cell[:, 1] *= 2 / crop_hr.shape[-1]

        return {
            'inp': crop_lr,
            'coord': hr_coord,
            'cell': cell,
            'gt': hr_rgb
        }


@register('sr-implicit-uniform-varied')
class SRImplicitUniformVaried(Dataset):
    #low_res, high_res given, downsampling uniformly varied
    def __init__(self, dataset, size_min, size_max=None,
                 augment=False, gt_resize=None, sample_q=None):
        self.dataset = dataset
        self.size_min = size_min
        if size_max is None:
            size_max = size_min
        self.size_max = size_max
        self.augment = augment
        self.gt_resize = gt_resize
        self.sample_q = sample_q

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        img_lr, img_hr = self.dataset[idx]
        p = idx / (len(self.dataset) - 1)
        w_hr = round(self.size_min + (self.size_max - self.size_min) * p)
        img_hr = resize_fn(img_hr, w_hr)

        if self.augment:
            if random.random() < 0.5:
                img_lr = img_lr.flip(-1)
                img_hr = img_hr.flip(-1)

        if self.gt_resize is not None:
            img_hr = resize_fn(img_hr, self.gt_resize)

        hr_coord, hr_rgb = to_pixel_samples(img_hr)

        if self.sample_q is not None:
            sample_lst = np.random.choice(
                len(hr_coord), self.sample_q, replace=False)
            hr_coord = hr_coord[sample_lst]
            hr_rgb = hr_rgb[sample_lst]

        cell = torch.ones_like(hr_coord)
        cell[:, 0] *= 2 / img_hr.shape[-2]
        cell[:, 1] *= 2 / img_hr.shape[-1]

        return {
            'inp': img_lr,
            'coord': hr_coord,
            'cell': cell,
            'gt': hr_rgb
        }