## Image Burst Enhancement with Neural Representations

This is the repository of the Bachelor thesis on Image Burst Enhancement with Neural Representations.

It follows the basic structure of https://yinboc.github.io/liif/. Code was adapted from them and from https://github.com/goutamgmb/deep-burst-sr.


### Environment
- Python 3.9
- Pytorch 1.13.0
- Tensorboard 2.11.0

For all other specific dependencies used on the training setup, the conda environment file 'conda_environment.yml' can be consulted or used to recreate the environment.

## Quick Start


## Reproducing Experiments

### Data
The dataset is from http://people.ee.ethz.ch/~ihnatova/pynet.html#dataset.
### Running the code
**0. Preliminaries**

- For train_liif.py' use '--gpu [GPU]' to specify the GPUs (e.g. '--gpu 0')

- For 'train_liif.py', by default, the save folder is at 'save/_[CONFIG_NAME]'. We can use '--name' to specify a name if needed.

- Note that by default, the dataset is first converted to binary files for the first time when running the architecture. If you don't want that, change the 'cache' parameter in the yaml configuration file to 'cache: in_memory' or 'cache: none' denotes direct loading.

**1. Experiments**
To rerun the trainings, one can use the config.yaml file given in the corresponding subfolder of 'save'.
Example: 
python3 /rawliif/liif_for_raw/train_liif.py --config /rawliif/liif_for_raw/save/experiment_11_rdnencoder/config.yaml --name /rerun_experiment_11/ --gpu 3

The baseline experiments are:
* experiment_11_rdnencoder (RDN encoder)
* experiment_12_res_replic_encoder (ResNet encoder)

The burst architecture experiments are:
* experiment_17_burstrdn* experiment_18_burstanetrdn* experiment_19_burstrdnanet* experiment_20_burstrdnanet_resume_exp_19

Note that experiment 20 is a continuation of experiment 19, because sleepy me pressed ctr-c killing the training script instead of cmd-q for quitting the terminal (TMUX for the win!).

**2. Validation runs**
For redoing the validations, one can run the deterministic_validation.py script as follows:
python /rawliif/liif_for_raw/deterministic_validation.py --config /rawliif/liif_for_raw/configs/config_burstanetrdn_experiment_18_deter_val.yaml --name /rawliif/liif_for_raw/save/deter_validation_experiment_19_best

**3. Visual validation**
For visual validation, one can use the visualizer_jupyter_notebook.ipynb Jupyter notebook.


**4. Appendix**
The liif_commented contains the code from https://yinboc.github.io/liif/, but comments were added.