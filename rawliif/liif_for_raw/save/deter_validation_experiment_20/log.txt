val dataset: size=320
  inp: shape=(4, 4, 48, 48)
  coord: shape=(9216, 2)
  cell: shape=(9216, 2)
  gt: shape=(9216, 3)
model: #params=24.8M
    no_upsampling: True
    G0: 64
    RDNkSize: 3
    RDNconfig: B
    nConv_anet: 3
    kernelsize_anet: 5
    padding_mode_anet: zeros
    ref_index_anet: 0
    burst_size: 4
epoch 0/100, val: psnr=13.4342
