train dataset: size=20000
  inp: shape=(4, 4, 48, 48)
  coord: shape=(2304, 2)
  cell: shape=(2304, 2)
  gt: shape=(2304, 3)
val dataset: size=320
  inp: shape=(4, 4, 48, 48)
  coord: shape=(2304, 2)
  cell: shape=(2304, 2)
  gt: shape=(2304, 3)
model: #params=22.9M
    no_upsampling: True
    G0: 64
    RDNkSize: 3
    RDNconfig: B
    burst_size_anet: 4
    nConv_anet: 3
    kernelsize_anet: 5
    padding_mode_anet: zeros
    ref_index_anet: 0
epoch 1/100, train: loss=0.0842, val: psnr=16.4599, 7.1m 7.1m/11.9h
epoch 2/100, train: loss=0.0476, val: psnr=17.2575, 7.1m 14.3m/11.9h
epoch 3/100, train: loss=0.0392, val: psnr=18.3646, 7.1m 21.4m/11.9h
epoch 4/100, train: loss=0.0347, val: psnr=18.8492, 7.1m 28.5m/11.9h
epoch 5/100, train: loss=0.0334, val: psnr=19.1731, 7.1m 35.6m/11.9h
epoch 6/100, train: loss=0.0326, val: psnr=19.3857, 7.1m 42.8m/11.9h
epoch 7/100, train: loss=0.0315, val: psnr=19.0086, 7.1m 49.9m/11.9h
epoch 8/100, train: loss=0.0315, val: psnr=19.2264, 7.1m 57.0m/11.9h
epoch 9/100, train: loss=0.0305, val: psnr=18.6969, 7.1m 1.1h/11.9h
epoch 10/100, train: loss=0.0304, val: psnr=19.6007, 7.1m 1.2h/11.9h
epoch 11/100, train: loss=0.0298, val: psnr=19.8691, 7.1m 1.3h/11.9h
epoch 12/100, train: loss=0.0286, val: psnr=20.0608, 7.1m 1.4h/11.9h
epoch 13/100, train: loss=0.0282, val: psnr=20.2572, 7.1m 1.5h/11.9h
epoch 14/100, train: loss=0.0274, val: psnr=21.0836, 7.1m 1.7h/11.9h
epoch 15/100, train: loss=0.0268, val: psnr=20.3385, 7.1m 1.8h/11.9h
epoch 16/100, train: loss=0.0267, val: psnr=20.9358, 7.1m 1.9h/11.9h
epoch 17/100, train: loss=0.0257, val: psnr=21.0201, 7.1m 2.0h/11.9h
epoch 18/100, train: loss=0.0253, val: psnr=20.8153, 7.1m 2.1h/11.9h
epoch 19/100, train: loss=0.0257, val: psnr=21.5827, 7.1m 2.3h/11.9h
epoch 20/100, train: loss=0.0244, val: psnr=21.2672, 7.1m 2.4h/11.9h
epoch 21/100, train: loss=0.0224, val: psnr=21.4608, 7.1m 2.5h/11.9h
epoch 22/100, train: loss=0.0227, val: psnr=21.1497, 7.1m 2.6h/11.9h
epoch 23/100, train: loss=0.0223, val: psnr=21.6316, 7.1m 2.7h/11.9h
epoch 24/100, train: loss=0.0223, val: psnr=21.8193, 7.1m 2.8h/11.9h
epoch 25/100, train: loss=0.0221, val: psnr=21.3991, 7.1m 3.0h/11.9h
epoch 26/100, train: loss=0.0219, val: psnr=21.8304, 7.1m 3.1h/11.9h
epoch 27/100, train: loss=0.0218, val: psnr=21.7506, 7.1m 3.2h/11.9h
epoch 28/100, train: loss=0.0216, val: psnr=21.8343, 7.1m 3.3h/11.9h
epoch 29/100, train: loss=0.0216, val: psnr=21.6973, 7.1m 3.4h/11.9h
epoch 30/100, train: loss=0.0214, val: psnr=22.3215, 7.1m 3.6h/11.9h
epoch 31/100, train: loss=0.0203, val: psnr=21.8699, 7.1m 3.7h/11.9h
epoch 32/100, train: loss=0.0203, val: psnr=22.0657, 7.1m 3.8h/11.9h
epoch 33/100, train: loss=0.0203, val: psnr=21.8626, 7.1m 3.9h/11.9h
epoch 34/100, train: loss=0.0202, val: psnr=22.1682, 7.1m 4.0h/11.9h
epoch 35/100, train: loss=0.0201, val: psnr=22.3927, 7.1m 4.1h/11.9h
epoch 36/100, train: loss=0.0200, val: psnr=22.4720, 7.1m 4.3h/11.9h
epoch 37/100, train: loss=0.0199, val: psnr=22.4667, 7.1m 4.4h/11.9h
epoch 38/100, train: loss=0.0199, val: psnr=22.1581, 7.1m 4.5h/11.9h
epoch 39/100, train: loss=0.0198, val: psnr=22.6678, 7.1m 4.6h/11.9h
epoch 40/100, train: loss=0.0197, val: psnr=22.8340, 7.1m 4.7h/11.9h
epoch 41/100, train: loss=0.0195, val: psnr=22.7642, 7.1m 4.9h/11.9h
epoch 42/100, train: loss=0.0194, val: psnr=22.3665, 7.1m 5.0h/11.9h
epoch 43/100, train: loss=0.0193, val: psnr=22.2758, 7.1m 5.1h/11.9h
epoch 44/100, train: loss=0.0194, val: psnr=22.2096, 7.1m 5.2h/11.9h
epoch 45/100, train: loss=0.0193, val: psnr=22.3458, 7.1m 5.3h/11.9h
epoch 46/100, train: loss=0.0192, val: psnr=22.6293, 7.1m 5.5h/11.9h
epoch 47/100, train: loss=0.0192, val: psnr=22.4103, 7.1m 5.6h/11.9h
epoch 48/100, train: loss=0.0191, val: psnr=22.2548, 7.1m 5.7h/11.9h
epoch 49/100, train: loss=0.0192, val: psnr=22.8224, 7.1m 5.8h/11.9h
epoch 50/100, train: loss=0.0191, val: psnr=22.3341, 7.1m 5.9h/11.9h
epoch 51/100, train: loss=0.0189, val: psnr=22.8871, 7.1m 6.0h/11.9h
epoch 52/100, train: loss=0.0188, val: psnr=22.6167, 7.1m 6.2h/11.9h
epoch 53/100, train: loss=0.0188, val: psnr=22.3093, 7.1m 6.3h/11.8h
epoch 54/100, train: loss=0.0189, val: psnr=22.1482, 7.1m 6.4h/11.8h
epoch 55/100, train: loss=0.0187, val: psnr=23.1174, 7.1m 6.5h/11.8h
epoch 56/100, train: loss=0.0189, val: psnr=22.6911, 7.1m 6.6h/11.8h
epoch 57/100, train: loss=0.0189, val: psnr=22.9193, 7.1m 6.8h/11.8h
epoch 58/100, train: loss=0.0186, val: psnr=22.5042, 7.1m 6.9h/11.8h
epoch 59/100, train: loss=0.0187, val: psnr=22.9383, 7.1m 7.0h/11.8h
epoch 60/100, train: loss=0.0188, val: psnr=22.2458, 7.1m 7.1h/11.8h
epoch 61/100, train: loss=0.0187, val: psnr=22.2898, 7.1m 7.2h/11.8h
epoch 62/100, train: loss=0.0187, val: psnr=22.1225, 7.1m 7.3h/11.8h
epoch 63/100, train: loss=0.0186, val: psnr=22.6014, 7.1m 7.5h/11.8h
epoch 64/100, train: loss=0.0187, val: psnr=22.5876, 7.1m 7.6h/11.8h
epoch 65/100, train: loss=0.0188, val: psnr=23.0802, 7.1m 7.7h/11.8h
epoch 66/100, train: loss=0.0187, val: psnr=22.8380, 7.1m 7.8h/11.8h
epoch 67/100, train: loss=0.0186, val: psnr=22.4860, 7.1m 7.9h/11.8h
epoch 68/100, train: loss=0.0187, val: psnr=22.7097, 7.1m 8.1h/11.8h
epoch 69/100, train: loss=0.0185, val: psnr=22.7356, 7.1m 8.2h/11.8h
epoch 70/100, train: loss=0.0186, val: psnr=22.2395, 7.1m 8.3h/11.8h
epoch 71/100, train: loss=0.0186, val: psnr=22.2313, 7.1m 8.4h/11.8h
epoch 72/100, train: loss=0.0185, val: psnr=22.6053, 7.1m 8.5h/11.8h
epoch 73/100, train: loss=0.0185, val: psnr=22.5210, 7.1m 8.6h/11.8h
epoch 74/100, train: loss=0.0185, val: psnr=22.8050, 7.1m 8.8h/11.8h
epoch 75/100, train: loss=0.0186, val: psnr=22.8328, 7.1m 8.9h/11.8h
epoch 76/100, train: loss=0.0184, val: psnr=23.1296, 7.1m 9.0h/11.8h
epoch 77/100, train: loss=0.0185, val: psnr=22.2012, 7.1m 9.1h/11.8h
epoch 78/100, train: loss=0.0184, val: psnr=22.7500, 7.1m 9.2h/11.8h
epoch 79/100, train: loss=0.0184, val: psnr=23.0953, 7.1m 9.4h/11.8h
epoch 80/100, train: loss=0.0185, val: psnr=22.9796, 7.1m 9.5h/11.8h
epoch 81/100, train: loss=0.0184, val: psnr=22.5982, 7.1m 9.6h/11.8h
epoch 82/100, train: loss=0.0184, val: psnr=22.5242, 7.1m 9.7h/11.8h
epoch 83/100, train: loss=0.0183, val: psnr=22.3382, 7.1m 9.8h/11.8h
epoch 84/100, train: loss=0.0182, val: psnr=22.4478, 7.1m 10.0h/11.8h
epoch 85/100, train: loss=0.0184, val: psnr=23.2530, 7.1m 10.1h/11.8h
epoch 86/100, train: loss=0.0183, val: psnr=22.8896, 7.1m 10.2h/11.8h
epoch 87/100, train: loss=0.0183, val: psnr=22.9032, 7.1m 10.3h/11.8h
epoch 88/100, train: loss=0.0183, val: psnr=22.4156, 7.1m 10.4h/11.8h
epoch 89/100, train: loss=0.0183, val: psnr=23.2906, 7.1m 10.5h/11.8h
epoch 90/100, train: loss=0.0183, val: psnr=23.1284, 7.1m 10.7h/11.8h
epoch 91/100, train: loss=0.0183, val: psnr=22.8850, 7.1m 10.8h/11.9h
epoch 92/100, train: loss=0.0183, val: psnr=22.9670, 7.1m 10.9h/11.9h
epoch 93/100, train: loss=0.0182, val: psnr=23.4736, 7.1m 11.0h/11.9h
epoch 94/100, train: loss=0.0182, val: psnr=22.8870, 7.1m 11.1h/11.9h
epoch 95/100, train: loss=0.0183, val: psnr=22.4414, 7.1m 11.3h/11.9h
epoch 96/100, train: loss=0.0182, val: psnr=23.2701, 7.1m 11.4h/11.9h
epoch 97/100, train: loss=0.0182, val: psnr=22.6336, 7.1m 11.5h/11.9h
epoch 98/100, train: loss=0.0182, val: psnr=22.8906, 7.1m 11.6h/11.9h
epoch 99/100, train: loss=0.0181, val: psnr=22.4818, 7.1m 11.7h/11.9h
epoch 100/100, train: loss=0.0181, val: psnr=22.5718, 7.1m 11.9h/11.9h
