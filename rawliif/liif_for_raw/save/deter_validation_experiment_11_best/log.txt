val dataset: size=320
  inp: shape=(4, 48, 48)
  coord: shape=(50176, 2)
  cell: shape=(50176, 2)
  gt: shape=(50176, 3)
model: #params=22.3M
    no_upsampling: True
epoch 0/500, val: psnr=35.4905
