#!/usr/bin/env python3

from sklearn.model_selection import train_test_split
import argparse
import random
import os.path
import json

parser = argparse.ArgumentParser()
parser.add_argument('--name') #path and filename
parser.add_argument('--train_size') #number of images in train set"
parser.add_argument('--test_size')   #number of images in test set
parser.add_argument('--random_seed') #random seed definition
args = parser.parse_args()

head, _ = os.path.split(args.name)

if int(args.train_size) + int(args.test_size) > 46839:
   raise Exception("Trainsize, " + args.train_size + " + Testsize, " + args.test_size + " greater than size of dataset, 46839")
if os.path.exists(head):
   pass
else: raise Exception("Path does not exist")
print(args)



dataset_index_list = list(range(int(args.train_size)+int(args.test_size)))
dataset_index_list = [str(i) for i in dataset_index_list]
x_train, x_test = train_test_split(dataset_index_list, test_size = float(args.test_size)/(int(args.train_size)+int(args.test_size)), random_state = int(args.random_seed))

dataset_dict = {"train" : x_train, "test" : x_test, "train_size" : args.train_size, "test_size" : args.test_size, "random_seed" : args.random_seed}


with open(args.name, "w") as fp:
   json.dump(dataset_dict, fp)