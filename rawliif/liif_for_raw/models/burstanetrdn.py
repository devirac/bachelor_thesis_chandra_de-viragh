# Residual Dense Network for Image Super-Resolution
# https://arxiv.org/abs/1802.08797
# modified from: https://github.com/thstkdgus35/EDSR-PyTorch

from argparse import Namespace

import torch
import torch.nn as nn

from models import register
from torchvision.ops import DeformConv2d

class RDB_Conv(nn.Module):
    def __init__(self, inChannels, growRate, kSize=3):
        super(RDB_Conv, self).__init__()
        Cin = inChannels
        G  = growRate
        self.conv = nn.Sequential(*[
            nn.Conv2d(Cin, G, kSize, padding=(kSize-1)//2, stride=1),
            nn.ReLU()
        ])
    def forward(self, x):
        out = self.conv(x)
        return torch.cat((x, out), 1)

class RDB(nn.Module):
    def __init__(self, growRate0, growRate, nConvLayers, kSize=3):
        super(RDB, self).__init__()
        G0 = growRate0
        G  = growRate
        C  = nConvLayers

        convs = []
        for c in range(C):
            convs.append(RDB_Conv(G0 + c*G, G))
        self.convs = nn.Sequential(*convs)

        # Local Feature Fusion
        self.LFF = nn.Conv2d(G0 + C*G, G0, 1, padding=0, stride=1)

    def forward(self, x): ##here deconv
        #x = devconf of x
        return self.LFF(self.convs(x)) + x

class RDN(nn.Module):
    def __init__(self, args):
        super(RDN, self).__init__()
        self.args = args
        r = args.scale[0]
        G0 = args.G0
        kSize = args.RDNkSize
        self.log_once = True

        # number of RDB blocks, conv layers, out channels
        self.D, C, G = {
            'A': (20, 6, 32),
            'B': (16, 8, 64),
        }[args.RDNconfig]

        # Shallow feature extraction net
        self.SFENet1 = nn.Conv2d(args.n_channels, G0, kSize, padding=(kSize-1)//2, stride=1)
        self.SFENet2 = nn.Conv2d(G0, G0, kSize, padding=(kSize-1)//2, stride=1)

        # Redidual dense blocks and dense feature fusion
        self.RDBs = nn.ModuleList()
        for i in range(self.D):
            self.RDBs.append(
                RDB(growRate0 = G0, growRate = G, nConvLayers = C)
            )

        # Global Feature Fusion
        self.GFF = nn.Sequential(*[
            nn.Conv2d(self.D * G0, G0, 1, padding=0, stride=1),
            nn.Conv2d(G0, G0, kSize, padding=(kSize-1)//2, stride=1)
        ])

        if args.no_upsampling:
            self.out_dim = G0
        else:
            self.out_dim = args.n_channels
            # Up-sampling net
            if r == 2 or r == 3:
                self.UPNet = nn.Sequential(*[
                    nn.Conv2d(G0, G * r * r, kSize, padding=(kSize-1)//2, stride=1),
                    nn.PixelShuffle(r),
                    nn.Conv2d(G, args.n_channels, kSize, padding=(kSize-1)//2, stride=1)
                ])
            elif r == 4:
                self.UPNet = nn.Sequential(*[
                    nn.Conv2d(G0, G * 4, kSize, padding=(kSize-1)//2, stride=1),
                    nn.PixelShuffle(2),
                    nn.Conv2d(G, G * 4, kSize, padding=(kSize-1)//2, stride=1),
                    nn.PixelShuffle(2),
                    nn.Conv2d(G, args.n_channels, kSize, padding=(kSize-1)//2, stride=1)
                ])
            else:
                raise ValueError("scale must be 2 or 3 or 4.")

    def forward(self, x):
        f__1 = self.SFENet1(x)
        #print(f__1.shape)
        x  = self.SFENet2(f__1)
        #print("x.shape", x.shape)
        RDBs_out = []
        for i in range(self.D):
            x = self.RDBs[i](x)
            RDBs_out.append(x)
        #print("x.shape", x.shape)
        x = self.GFF(torch.cat(RDBs_out,1))
        #print("x.shape", x.shape)
        x += f__1
        if self.args.no_upsampling:
            #print("encoded output shape", x.shape)
            return x
        else:
            #print("encoded output shape", x.shape)
            return self.UPNet(x)


class ABlock(nn.Module):
    def __init__(self, outChannels, nConv, kernelsize, padding_mode, log_once):
        super().__init__()
        self.seq_list = nn.ModuleList()
        self.nConv = nConv
        self.padding_mode = padding_mode
        self.outChannels = outChannels
        self.log_once = log_once

        for i in range(self.nConv-1):
            if i == 0: #initial layer
                self.seq_list.append(nn.Conv2d(in_channels = 8, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode))
                self.seq_list.append(nn.ReLU())
            else: #following layers
                self.seq_list.append(nn.Conv2d(in_channels = outChannels, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode))
                self.seq_list.append(nn.ReLU())

        self.seq_list.append(nn.Conv2d(in_channels = outChannels, out_channels = 3*self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode))
        self.seq_list.append(nn.ReLU())
        self.seq_list.append(DeformConv2d(in_channels=self.outChannels, out_channels=self.outChannels, kernel_size = 1))
        
    def forward(self, ref, frame):
        x = torch.concat((ref,frame), 2).squeeze(1)
        identity = x.repeat(1,int(self.outChannels/x.shape[1]), 1, 1)
        print('identity.shape = ', identity.shape)
        out = x
        if self.log_once == True:
            print("shape of input to anet: ", out.shape)
        for i, layer in enumerate(self.seq_list):
            if self.log_once == True:
                print(i, "th layer: shape after layer: ", out.shape)
            #if deform conv layer:
            if (i == len(self.seq_list)-3):
                print("shape. out ", out.shape)
                out = out + identity
            if (i == len(self.seq_list)-1):
                #print("shape input for deconv: ", out[0:self.outChannels,...].shape, out[self.outChannels:3*self.outChannels,...].shape)
                if len(out.shape) == 3:
                    out = out.unsqueeze(0)
                    out = layer(out[:,0:self.outChannels,...], out[:,self.outChannels:3*self.outChannels,...])
                    out = out.squeeze()
                else: out = layer(out[:,0:self.outChannels,...], out[:,self.outChannels:3*self.outChannels,...])
            else: out = layer(out)
        self.log_once = False
        return out

class ANet(nn.Module):
    def __init__(self, args):
        super().__init__()
        self.args = args
        self.ref_index = args.ref_index_anet
        self.burst_size = args.burst_size_anet
        outChannels = args.G0
        nConv = args.nConv_anet
        kernelsize = args.kernelsize_anet
        padding_mode = args.padding_mode_anet
        self.log_once = True
        self.A = ABlock(outChannels = outChannels, nConv = nConv, kernelsize = kernelsize, padding_mode = padding_mode, log_once = self.log_once)


    def forward(self, burst):
        out = []
        if len(burst.shape) == 4: #if no batch dimension
            chunks = [chunk for chunk in burst.unsqueeze(0).chunk(self.burst_size, dim = 1)] #make chunks along burst dimension
        else:
            chunks = [chunk for chunk in burst.chunk(self.burst_size, dim = 1)] #make chunks along burst dimension
        for chunk in chunks:
            if self.log_once == True:
                print("chunk shape: ", chunk.shape)
            out.append(self.A(chunks[self.ref_index], chunk))
            self.log_once = False
        #print("torch.cat(out, dim=1).shape: ", torch.cat(out, dim=1).shape)

        #if no batch_dimension
        if len(out[0].shape) == 3:
            return torch.cat(out, dim=0).unsqueeze(0)
        else: return torch.cat(out, dim=1)

class BurstANETRDN(nn.Module):
    def __init__(self, args):
        super().__init__()
        self.RDN = RDN(args)
        self.ANET = ANet(args)
        self.out_dim = args.G0
    def forward(self, burst):
        aligned = self.ANET(burst)
        print("aligned", aligned.shape)
        out = self.RDN(aligned)
        return out


@register('burstanetrdn')
def make_burstanetrdn(G0=64, RDNkSize=3, RDNconfig='B',scale=2, no_upsampling=True,
                    nConv_anet = 3, burst_size_anet = 5, kernelsize_anet = 5, padding_mode_anet = 'zeros', ref_index_anet = 0):
    args = Namespace()
    #args needed for anet
    args.nConv_anet = nConv_anet
    args.burst_size_anet = burst_size_anet
    args.kernelsize_anet = kernelsize_anet
    args.padding_mode_anet = padding_mode_anet
    args.ref_index_anet = ref_index_anet

    #args needed for rdn
    args.G0 = G0
    args.RDNkSize = RDNkSize
    args.RDNconfig = RDNconfig
    args.scale = [scale]
    args.no_upsampling = no_upsampling
    args.n_channels = G0*burst_size_anet

    return BurstANETRDN(args)

