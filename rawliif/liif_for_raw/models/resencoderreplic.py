# Copyright (c) 2021 Huawei Technologies Co., Ltd.
# Licensed under CC BY-NC-SA 4.0 (Attribution-NonCommercial-ShareAlike 4.0 International) (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
#
# The code is released for academic research use only. For commercial use, please contact Huawei Technologies Co., Ltd.
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Code adapted from https://github.com/goutamgmb/deep-burst-sr

import torch
import torch.nn as nn
from . import blocks
from models import register
from argparse import Namespace


class ResEncoder(nn.Module):
    """ Encodes the input images using a residual network.
    """
    def __init__(self, init_dim, num_res_blocks,
                 out_dim, use_bn=False, activation='relu'):
        super().__init__()
        input_channels = 4
        self.init_dim = init_dim
        self.num_res_blocks = num_res_blocks
        self.out_dim = out_dim
        

        self.init_layer = blocks.conv_block(input_channels, init_dim, 3, stride=1, padding=1, padding_mode="replicate", batch_norm=use_bn,
                                            activation=activation)

        res_layers = []
        for _ in range(num_res_blocks):
            res_layers.append(blocks.ResBlock(init_dim, init_dim, stride=1, batch_norm=use_bn, activation=activation))

        self.res_layers = nn.Sequential(*res_layers)

        self.out_layer = blocks.conv_block(init_dim, out_dim, 3, stride=1, padding=1, batch_norm=use_bn,
                                           activation=activation)

    def forward(self, x):
        out = self.init_layer(x)
        feat = self.res_layers(out)
        feat = self.out_layer(out)
        return feat

@register('resencoderreplic')
def make_resencoderreplic(init_dim, num_res_blocks=64,
                 out_dim=512, use_bn=False, activation='relu'):
    
    return ResEncoder(init_dim, num_res_blocks,
                 out_dim, use_bn, activation)