import torch
import torch.nn as nn
from torchvision.ops import DeformConv2d

from models import register
#from torch import autograd
#autograd.set_detect_anomaly(True)

class ABlock(nn.Module):
    def __init__(self, outChannels, nConv, kernelsize, padding_mode):
        super().__init__()
        self.seq_list = []
        self.nConv = nConv #not used
        self.padding_mode = padding_mode
        self.outChannels = outChannels

        # for i in range(self.nConv-1):
        #     if i == 0: #initial layer
        #         self.seq_list.append(nn.Conv2d(in_channels = 8, out_channels = self.outChannels, kernel_size = kernelsize, padding_mode=self.padding_mode))
        #         self.seq_list.append(nn.ReLU())
        #     else: #following layers
        #         self.seq_list.append(nn.Conv2d(in_channels = outChannels, out_channels = self.outChannels, kernel_size = kernelsize, padding_mode=self.padding_mode))
        #         self.seq_list.append(nn.ReLU())

        self.conv1 = nn.Conv2d(in_channels = 8, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode)
        self.conv2 = nn.Conv2d(in_channels = outChannels, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode)
        self.conv3 = nn.Conv2d(in_channels = outChannels, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode)
        self.last_conv = nn.Conv2d(in_channels = outChannels, out_channels = 3*self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode)
        self.deform_conv = DeformConv2d(in_channels= self.outChannels, out_channels=self.outChannels, kernel_size = 1)
        self.relu = nn.ReLU()
        
    def forward(self, ref, frame):
        print("shape concat frame: ", frame.shape)
        print("shape concat ref: ", ref.shape)
        x = torch.concat((ref,frame), 1)
        print("shape concat x: ", x.shape)
        # shape concat:  torch.Size([4, 8, 50, 50])
        identity = x.repeat(1,int(self.outChannels/8), 1, 1)
        print("shape identity: ", identity.shape)
        # shape identity:  torch.Size([4, 32, 50, 50])
        out = x
        out = self.conv1(out)
        print("shape after conv1", out.shape)
        # shape after conv1 torch.Size([4, 32, 50, 50])
        out = self.relu(out)
        out = self.conv2(out)
        print("shape after conv2", out.shape)
        # shape after conv2 torch.Size([4, 32, 50, 50])
        out = self.relu(out)
        out = self.conv3(out)
        print("shape after conv3", out.shape)
        # shape after conv3 torch.Size([4, 32, 50, 50])
        out = self.relu(out)
        print("out shape before addition with identity", out.shape)
        # out shape before addition with identity torch.Size([4, 32, 50, 50])
        out = identity + out
        out = self.last_conv(out)
        print("shape after last_conv", out.shape)
        # shape after last_conv torch.Size([4, 96, 50, 50])
        out = self.relu(out)
        print("input to deform: out.shape = ", out[:,0:self.outChannels,:,:].shape, ", offset.shape= ", out[:,self.outChannels:3*self.outChannels].shape)
        #input to deform: out.shape =  torch.Size([4, 32, 50, 50]) , offset.shape=  torch.Size([4, 64, 50, 50]
        out = self.deform_conv(out[:,0:self.outChannels,:,:], out[:,self.outChannels:3*self.outChannels])
        return out

class ANet(nn.Module):
    def __init__(self, outChannels, padding_mode, nConv, burst_size, kernelsize=5, ref_index = 0):
        super().__init__()
        self.ref_index = ref_index
        self.out_dim = outChannels*burst_size
        self.burst_size = burst_size
        self.A = ABlock(outChannels = outChannels, nConv= nConv, kernelsize=kernelsize, padding_mode=padding_mode)
    
    def forward(self, burst):
        print("forward anet: burst.shape: ", burst.shape)
        #out = torch.Tensor().cuda()
        #for i in range(burst.shape[1]):
        #   out = torch.concat((self.A(burst[:,0,...],burst[:,i,...]),out), 1)
        #   print("out.shape: ", out.shape)
        #return out
        out = []
        chunks = [chunk.squeeze() for chunk in burst.chunk(self.burst_size, dim = 1)]
        for chunk in chunks:
           print("chunk shape: ", chunk.shape)
           out.append(self.A(chunks[self.ref_index], chunk))
        return torch.cat(out, dim=1)
        #return torch.concat((self.A(burst[:,0,...],burst[:,0,...]), self.A(burst[:,0,...],burst[:,1,...]), self.A(burst[:,0,...],burst[:,2,...]), self.A(burst[:,0,...],burst[:,3,...]), self.A(burst[:,0,...],burst[:,4,...])), 1)

@register('anet')
def make_anet(outChannels, nConv, burst_size, kernelsize, padding_mode, ref_index):

    return ANet(outChannels=outChannels, nConv=nConv, burst_size = burst_size, kernelsize=kernelsize, padding_mode=padding_mode, ref_index = ref_index)




class ABlockV2(nn.Module):
    def __init__(self, outChannels, nConv, kernelsize, padding_mode, log_once):
        super().__init__()
        self.seq_list = nn.ModuleList()
        self.nConv = nConv
        self.padding_mode = padding_mode
        self.outChannels = outChannels
        self.log_once = log_once

        for i in range(self.nConv-1):
            if i == 0: #initial layer
                self.seq_list.append(nn.Conv2d(in_channels = 8, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode))
                self.seq_list.append(nn.ReLU())
            else: #following layers
                self.seq_list.append(nn.Conv2d(in_channels = outChannels, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode))
                self.seq_list.append(nn.ReLU())

        # self.conv1 = nn.Conv2d(in_channels = 8, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode)
        # self.conv2 = nn.Conv2d(in_channels = outChannels, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode)
        # self.conv3 = nn.Conv2d(in_channels = outChannels, out_channels = self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode)
        self.seq_list.append(nn.Conv2d(in_channels = outChannels, out_channels = 3*self.outChannels, kernel_size = kernelsize, padding = 2, padding_mode=self.padding_mode))
        self.seq_list.append(nn.ReLU())
        self.seq_list.append(DeformConv2d(in_channels=self.outChannels, out_channels=self.outChannels, kernel_size = 1))
        self.seq_list.append(nn.ReLU())
        
    def forward(self, ref, frame):
        x = torch.concat((ref,frame), 1)
        out = x
        if self.log_once == True:
            print("shape of input to net: ", out.shape)
        for i, layer in enumerate(self.seq_list):
            if self.log_once == True:
                print(i, "th layer: shape after layer: ", out.shape)
            #if deform conv layer:
            if (i == len(self.seq_list)-2):
                out = layer(out[:,0:self.outChannels,...], out[:,self.outChannels:3*self.outChannels, ...])
            else: out = layer(out)
        self.log_once = False
        return out

class ANetV2(nn.Module):
    def __init__(self, outChannels, padding_mode, nConv, burst_size, kernelsize=5, ref_index = 0):
        super().__init__()
        self.ref_index = ref_index
        self.out_dim = outChannels*burst_size
        self.burst_size = burst_size
        self.log_once = True
        self.A = ABlockV2(outChannels = outChannels, nConv = nConv, kernelsize = kernelsize, padding_mode = padding_mode, log_once = self.log_once)
    
    def forward(self, burst):
        out = []
        chunks = [chunk.squeeze() for chunk in burst.chunk(self.burst_size, dim = 1)]
        for chunk in chunks:
            if self.log_once == True:
                print("chunk shape: ", chunk.shape)
            out.append(self.A(chunks[self.ref_index], chunk))
            self.log_once = False
        print("torch.cat(out, dim=1).shape: ", torch.cat(out, dim=1).shape)
        return torch.cat(out, dim=1)

@register('anetv2')
def make_anetv2(outChannels, nConv, burst_size, kernelsize, padding_mode, ref_index):
    return ANetV2(outChannels=outChannels, nConv=nConv, burst_size = burst_size, kernelsize=kernelsize, padding_mode=padding_mode, ref_index = ref_index)